from PIL import Image
import sys

import pyocr
import pyocr.builders

#sets up the language and toool to read in

toolList = pyocr.get_available_tools()
if len(toolList) == 0:
    print("No OCR tool found")
    sys.exit(1)
tool = toolList[0]
print("Will use tool '%s'" % (tool.get_name()))
langs = tool.get_available_languages()
print("Available languages: %s" % ", ".join(langs))
lang = langs[0]
print("Will use lang '%s'" % (lang))

txt = tool.image_to_string(
    Image.open("henlo"),
    lang=lang,
    builder=pyocr.builders.TextBuilder())

